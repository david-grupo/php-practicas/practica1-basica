<!DOCTYPE html>
<html>
    <head><meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function calculoColor(){
            $color="rgb(".rand(0,255).", ".rand(0,255).", ".rand(0,255). ")";
            return $color;
        }
        
        function dibujarCirculo1($x,$y){
            ?>
        <circle cx="<?= $x ?>" cy="<?= $y ?>" r="50" fill="<?= calculoColor()?>" />
        <?php
        }
        
        function dibujarCirculo ($x,$y){
            echo '<circle cx="'.$x.'" cy="'.$y.'" r="50" fill="'.calculoColor().'"/>';
        }
        ?>
        <svg version="1.1" xmlns="http:/>/www.w3.org/2000/svg" width="1000px" height="1000px" 
             style="display:block;margin:0px auto;">
        <?php
        dibujarCirculo(50,50);
        dibujarCirculo(150,50);
        dibujarCirculo(250,250);
        ?>
        </svg>
        
    </body>
</html>


